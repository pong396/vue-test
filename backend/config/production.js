module.exports = {
  env: 'production',

  //database: {
  //  username: 'postgres',
  //  dialect: 'postgres',
  //  password: '',
  //  database: 'postgres',
  //  host: 'postgres',
  //  logging: console.log,
  //  operatorsAliases: false,
  //},

   database: {
     username: 'dbmasteruser',
     dialect: 'mysql',
     password: ':ReVA2u%`zaBl5tgrxnwU{G}H!YElldt',
     database: 'WebCRMDB',
     host: 'ls-3fdd37256a4b3d7d7e7feb2bcfc71bc73a4b4a96.cigblwaydivs.ap-southeast-1.rds.amazonaws.com',
     logging: console.log,
     operatorsAliases: false,
   },

  email: {
    comment: 'See https://nodemailer.com',
    from: '<insert your email here>',
    host: null,
    auth: {
      user: null,
      pass: null,
    },
  },

  graphiql: false,

  clientUrl:
    '<insert client url here>',

  defaultUser: null,

  uploadDir: '/storage',

  authJwtSecret: '<place a generated random value here>',
};
