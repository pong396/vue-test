const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  const compound = sequelize.define(
    'compound',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        }
      },
      unit: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: [
          "IU",
          "mg",
          "mcg"
        ],
      },
      standardDose: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {

        }
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  compound.associate = (models) => {




    models.compound.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.compound.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return compound;
};
