const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  const compoundLot = sequelize.define(
    'compoundLot',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },

      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  compoundLot.associate = (models) => {
    models.compoundLot.belongsTo(models.vendor, {
      as: 'vendorId',
      constraints: false,
    });

    models.compoundLot.belongsTo(models.compound, {
      as: 'compoundId',
      constraints: false,
    });



    models.compoundLot.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.compoundLot.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return compoundLot;
};
