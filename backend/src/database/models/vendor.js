const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  const vendor = sequelize.define(
    'vendor',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.TEXT,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  vendor.associate = (models) => {




    models.vendor.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.vendor.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return vendor;
};
