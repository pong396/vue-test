module.exports = [
  require('./compoundLot'),
  require('./compoundLotInput'),
  require('./compoundLotFilterInput'),
  require('./compoundLotOrderByEnum'),
  require('./compoundLotPage'),
  require('./compoundLotEnums'),
];
