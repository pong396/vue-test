const schema = `
  type CompoundLotPage {
    rows: [CompoundLot!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
