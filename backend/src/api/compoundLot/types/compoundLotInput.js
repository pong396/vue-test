const schema = `
  input CompoundLotInput {
    vendorId: String!
    compoundId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
