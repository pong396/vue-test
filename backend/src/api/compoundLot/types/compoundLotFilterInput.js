const schema = `
  input CompoundLotFilterInput {
    id: String

    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
