const schema = `
  type CompoundLot {
    id: String!
    vendorId: Vendor
    compoundId: Compound
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
