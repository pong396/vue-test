const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundLotImport(data: CompoundLotInput!, importHash: String!): Boolean
`;

const resolver = {
  compoundLotImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotImport);

    await new CompoundLotService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
