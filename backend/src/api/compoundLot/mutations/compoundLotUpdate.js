const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundLotUpdate(id: String!, data: CompoundLotInput!): CompoundLot!
`;

const resolver = {
  compoundLotUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotEdit);

    return new CompoundLotService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
