const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundLotDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  compoundLotDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotDestroy);

    await new CompoundLotService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
