const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundLotCreate(data: CompoundLotInput!): CompoundLot!
`;

const resolver = {
  compoundLotCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotCreate);

    return new CompoundLotService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
