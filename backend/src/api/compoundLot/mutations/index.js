module.exports = [
  require('./compoundLotCreate'),
  require('./compoundLotDestroy'),
  require('./compoundLotUpdate'),
  require('./compoundLotImport'),
];
