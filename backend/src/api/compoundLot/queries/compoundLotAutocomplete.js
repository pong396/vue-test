const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const CompoundLotService = require('../../../services/compoundLotService');

const schema = `
  compoundLotAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  compoundLotAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotAutocomplete);

    return new CompoundLotService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
