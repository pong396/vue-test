const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  compoundLotList(filter: CompoundLotFilterInput, limit: Int, offset: Int, orderBy: CompoundLotOrderByEnum): CompoundLotPage!
`;

const resolver = {
  compoundLotList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotRead);

    return new CompoundLotService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
