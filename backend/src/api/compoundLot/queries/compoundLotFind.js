const CompoundLotService = require('../../../services/compoundLotService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundLotFind(id: String!): CompoundLot!
`;

const resolver = {
  compoundLotFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundLotRead);

    return new CompoundLotService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
