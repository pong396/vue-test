module.exports = [
  require('./compoundLotFind'),
  require('./compoundLotList'),
  require('./compoundLotAutocomplete'),
];
