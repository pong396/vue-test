const mergeResolvers = require('./shared/utils/mergeGraphqlResolvers');

const sharedTypes = require('./shared/types');

const settingsTypes = require('./settings/types');
const settingsQueries = require('./settings/queries');
const settingsMutations = require('./settings/mutations');

const authTypes = require('./auth/types');
const authQueries = require('./auth/queries');
const authMutations = require('./auth/mutations');

const iamTypes = require('./iam/types');
const iamQueries = require('./iam/queries');
const iamMutations = require('./iam/mutations');

const auditLogTypes = require('./auditLog/types');
const auditLogQueries = require('./auditLog/queries');
const auditLogMutations = require('./auditLog/mutations');

const vendorTypes = require('./vendor/types');
const vendorQueries = require('./vendor/queries');
const vendorMutations = require('./vendor/mutations');

const compoundTypes = require('./compound/types');
const compoundQueries = require('./compound/queries');
const compoundMutations = require('./compound/mutations');

const compoundLotTypes = require('./compoundLot/types');
const compoundLotQueries = require('./compoundLot/queries');
const compoundLotMutations = require('./compoundLot/mutations');

const customerTypes = require('./customer/types');
const customerQueries = require('./customer/queries');
const customerMutations = require('./customer/mutations');

const productTypes = require('./product/types');
const productQueries = require('./product/queries');
const productMutations = require('./product/mutations');

const orderTypes = require('./order/types');
const orderQueries = require('./order/queries');
const orderMutations = require('./order/mutations');

const types = [
  ...sharedTypes,
  ...iamTypes,
  ...authTypes,
  ...auditLogTypes,
  ...settingsTypes,
  ...vendorTypes,
  ...compoundTypes,
  ...compoundLotTypes,
  ...customerTypes,
  ...productTypes,
  ...orderTypes,
].map((type) => type.resolver);

const queries = [
  ...iamQueries,
  ...authQueries,
  ...auditLogQueries,
  ...settingsQueries,
  ...vendorQueries,
  ...compoundQueries,
  ...compoundLotQueries,
  ...customerQueries,
  ...productQueries,
  ...orderQueries,
].map((query) => query.resolver);

const mutations = [
  ...iamMutations,
  ...authMutations,
  ...auditLogMutations,
  ...settingsMutations,
  ...vendorMutations,
  ...compoundMutations,
  ...compoundLotMutations,
  ...customerMutations,
  ...productMutations,
  ...orderMutations,
].map((mutation) => mutation.resolver);

module.exports = mergeResolvers(types, queries, mutations);
