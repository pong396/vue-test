const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundCreate(data: CompoundInput!): Compound!
`;

const resolver = {
  compoundCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundCreate);

    return new CompoundService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
