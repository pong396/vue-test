module.exports = [
  require('./compoundCreate'),
  require('./compoundDestroy'),
  require('./compoundUpdate'),
  require('./compoundImport'),
];
