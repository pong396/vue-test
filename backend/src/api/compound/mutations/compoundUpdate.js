const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundUpdate(id: String!, data: CompoundInput!): Compound!
`;

const resolver = {
  compoundUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundEdit);

    return new CompoundService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
