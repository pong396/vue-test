const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  compoundDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundDestroy);

    await new CompoundService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
