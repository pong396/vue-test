const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundImport(data: CompoundInput!, importHash: String!): Boolean
`;

const resolver = {
  compoundImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundImport);

    await new CompoundService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
