const schema = `
  enum CompoundUnitEnum {
    IU
    mg
    mcg
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
