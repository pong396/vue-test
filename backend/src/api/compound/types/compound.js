const schema = `
  type Compound {
    id: String!
    name: String
    unit: CompoundUnitEnum
    standardDose: Int
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
