const schema = `
  type CompoundPage {
    rows: [Compound!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
