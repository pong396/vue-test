const schema = `
  input CompoundFilterInput {
    id: String
    name: String
    unit: CompoundUnitEnum
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
