const schema = `
  input CompoundInput {
    name: String!
    unit: CompoundUnitEnum!
    standardDose: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
