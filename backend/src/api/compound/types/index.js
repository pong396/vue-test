module.exports = [
  require('./compound'),
  require('./compoundInput'),
  require('./compoundFilterInput'),
  require('./compoundOrderByEnum'),
  require('./compoundPage'),
  require('./compoundEnums'),
];
