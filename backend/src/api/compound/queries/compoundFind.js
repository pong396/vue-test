const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  compoundFind(id: String!): Compound!
`;

const resolver = {
  compoundFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundRead);

    return new CompoundService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
