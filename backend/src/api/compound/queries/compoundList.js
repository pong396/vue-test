const CompoundService = require('../../../services/compoundService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  compoundList(filter: CompoundFilterInput, limit: Int, offset: Int, orderBy: CompoundOrderByEnum): CompoundPage!
`;

const resolver = {
  compoundList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundRead);

    return new CompoundService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
