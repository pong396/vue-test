module.exports = [
  require('./compoundFind'),
  require('./compoundList'),
  require('./compoundAutocomplete'),
];
