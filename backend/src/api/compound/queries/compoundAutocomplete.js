const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const CompoundService = require('../../../services/compoundService');

const schema = `
  compoundAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  compoundAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.compoundAutocomplete);

    return new CompoundService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
