const schema = `
  type VendorPage {
    rows: [Vendor!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
