module.exports = [
  require('./vendor'),
  require('./vendorInput'),
  require('./vendorFilterInput'),
  require('./vendorOrderByEnum'),
  require('./vendorPage'),
  require('./vendorEnums'),
];
