const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  vendorUpdate(id: String!, data: VendorInput!): Vendor!
`;

const resolver = {
  vendorUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorEdit);

    return new VendorService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
