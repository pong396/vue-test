module.exports = [
  require('./vendorCreate'),
  require('./vendorDestroy'),
  require('./vendorUpdate'),
  require('./vendorImport'),
];
