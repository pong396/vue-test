const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  vendorImport(data: VendorInput!, importHash: String!): Boolean
`;

const resolver = {
  vendorImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorImport);

    await new VendorService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
