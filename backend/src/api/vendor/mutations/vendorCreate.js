const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  vendorCreate(data: VendorInput!): Vendor!
`;

const resolver = {
  vendorCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorCreate);

    return new VendorService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
