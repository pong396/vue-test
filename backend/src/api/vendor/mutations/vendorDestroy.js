const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  vendorDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  vendorDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorDestroy);

    await new VendorService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
