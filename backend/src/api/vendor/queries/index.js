module.exports = [
  require('./vendorFind'),
  require('./vendorList'),
  require('./vendorAutocomplete'),
];
