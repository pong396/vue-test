const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const VendorService = require('../../../services/vendorService');

const schema = `
  vendorAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  vendorAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorAutocomplete);

    return new VendorService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
