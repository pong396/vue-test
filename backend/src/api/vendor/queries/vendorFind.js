const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  vendorFind(id: String!): Vendor!
`;

const resolver = {
  vendorFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorRead);

    return new VendorService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
