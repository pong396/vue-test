const VendorService = require('../../../services/vendorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  vendorList(filter: VendorFilterInput, limit: Int, offset: Int, orderBy: VendorOrderByEnum): VendorPage!
`;

const resolver = {
  vendorList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.vendorRead);

    return new VendorService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
