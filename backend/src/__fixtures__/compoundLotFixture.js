const genericFixture = require('./genericFixture');
const CompoundLotRepository = require('../database/repositories/compoundLotRepository');

const compoundLotFixture = genericFixture({
  idField: 'id',
  createFn: (data) => new CompoundLotRepository().create(data),
  data: [
    {
      id: '1',
      // Add attributes here
    },
  ],
});

module.exports = compoundLotFixture;
