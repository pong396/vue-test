const userFixture = require('./userFixture');
const vendorFixture = require('./vendorFixture');
const compoundFixture = require('./compoundFixture');
const compoundLotFixture = require('./compoundLotFixture');
const customerFixture = require('./customerFixture');
const productFixture = require('./productFixture');
const orderFixture = require('./orderFixture');
const AbstractRepository = require('../database/repositories/abstractRepository');

module.exports = {
  user: userFixture,
  vendor: vendorFixture,
  compound: compoundFixture,
  compoundLot: compoundLotFixture,
  customer: customerFixture,
  product: productFixture,
  order: orderFixture,

  async cleanDatabase() {
    await AbstractRepository.cleanDatabase();
  },
};
