const genericFixture = require('./genericFixture');
const VendorRepository = require('../database/repositories/vendorRepository');

const vendorFixture = genericFixture({
  idField: 'id',
  createFn: (data) => new VendorRepository().create(data),
  data: [
    {
      id: '1',
      // Add attributes here
    },
  ],
});

module.exports = vendorFixture;
