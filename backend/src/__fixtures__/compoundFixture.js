const genericFixture = require('./genericFixture');
const CompoundRepository = require('../database/repositories/compoundRepository');

const compoundFixture = genericFixture({
  idField: 'id',
  createFn: (data) => new CompoundRepository().create(data),
  data: [
    {
      id: '1',
      // Add attributes here
    },
  ],
});

module.exports = compoundFixture;
