class Roles {
  static get values() {
    return {
      owner: 'owner',
      editor: 'editor',
      viewer: 'viewer',
      auditLogViewer: 'auditLogViewer',
      iamSecurityReviewer: 'iamSecurityReviewer',
      entityEditor: 'entityEditor',
      entityViewer: 'entityViewer',
      vendorEditor: 'vendorEditor',
      vendorViewer: 'vendorViewer',
      compoundEditor: 'compoundEditor',
      compoundViewer: 'compoundViewer',
      compoundLotEditor: 'compoundLotEditor',
      compoundLotViewer: 'compoundLotViewer',
      customerEditor: 'customerEditor',
      customerViewer: 'customerViewer',
      productEditor: 'productEditor',
      productViewer: 'productViewer',
      orderEditor: 'orderEditor',
      orderViewer: 'orderViewer',
    };
  }
}

module.exports = Roles;
