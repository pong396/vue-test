import routes from '@/modules/product/product-routes';
import store from '@/modules/product/product-store';

export default {
  routes,
  store,
};
