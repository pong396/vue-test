import gql from 'graphql-tag';
import graphqlClient from '@/shared/graphql/client';

export class VendorService {
  static async update(id, data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation VENDOR_UPDATE(
          $id: String!
          $data: VendorInput!
        ) {
          vendorUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.vendorUpdate;
  }

  static async destroyAll(ids) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation VENDOR_DESTROY($ids: [String!]!) {
          vendorDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.vendorDestroy;
  }

  static async create(data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation VENDOR_CREATE($data: VendorInput!) {
          vendorCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.vendorCreate;
  }

  static async import(values, importHash) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation VENDOR_IMPORT(
          $data: VendorInput!
          $importHash: String!
        ) {
          vendorImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.vendorImport;
  }

  static async find(id) {
    const response = await graphqlClient.query({
      query: gql`
        query VENDOR_FIND($id: String!) {
          vendorFind(id: $id) {
            id
            name
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.vendorFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const response = await graphqlClient.query({
      query: gql`
        query VENDOR_LIST(
          $filter: VendorFilterInput
          $orderBy: VendorOrderByEnum
          $limit: Int
          $offset: Int
        ) {
          vendorList(
            filter: $filter
            orderBy: $orderBy
            limit: $limit
            offset: $offset
          ) {
            count
            rows {
              id
              name
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.vendorList;
  }

  static async listAutocomplete(query, limit) {
    const response = await graphqlClient.query({
      query: gql`
        query VENDOR_AUTOCOMPLETE(
          $query: String
          $limit: Int
        ) {
          vendorAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.vendorAutocomplete;
  }
}
