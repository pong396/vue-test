import importerStore from '@/shared/importer/importer-store';
import { VendorService } from '@/modules/vendor/vendor-service';
import vendorImporterFields from '@/modules/vendor/vendor-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  VendorService.import,
  vendorImporterFields,
  i18n('entities.vendor.importer.fileName'),
  i18n('entities.vendor.importer.hint'),
);
