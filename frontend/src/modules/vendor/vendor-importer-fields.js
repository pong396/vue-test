import { VendorModel } from '@/modules/vendor/vendor-model';

const { fields } = VendorModel;

export default [
  fields.name,
];
