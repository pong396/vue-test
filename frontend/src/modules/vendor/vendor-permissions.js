import Permissions from '@/security/permissions';
import PermissionChecker from '@/modules/iam/permission-checker';

export class VendorPermissions {
  constructor(currentUser) {
    const permissionChecker = new PermissionChecker(
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.vendorRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.vendorImport,
    );
    this.vendorAutocomplete = permissionChecker.match(
      Permissions.values.vendorAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.vendorCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.vendorEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.vendorDestroy,
    );
  }
}
