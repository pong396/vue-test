import vendorListStore from '@/modules/vendor/vendor-list-store';
import vendorViewStore from '@/modules/vendor/vendor-view-store';
import vendorImporterStore from '@/modules/vendor/vendor-importer-store';
import vendorFormStore from '@/modules/vendor/vendor-form-store';
import vendorDestroyStore from '@/modules/vendor/vendor-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: vendorDestroyStore,
    form: vendorFormStore,
    list: vendorListStore,
    view: vendorViewStore,
    importer: vendorImporterStore,
  },
};
