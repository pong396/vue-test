import routes from '@/modules/vendor/vendor-routes';
import store from '@/modules/vendor/vendor-store';

export default {
  routes,
  store,
};
