import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const VendorListPage = () =>
  import('@/modules/vendor/components/vendor-list-page.vue');
const VendorFormPage = () =>
  import('@/modules/vendor/components/vendor-form-page.vue');
const VendorViewPage = () =>
  import('@/modules/vendor/components/vendor-view-page.vue');
const VendorImporterPage = () =>
  import('@/modules/vendor/components/vendor-importer-page.vue');

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'vendor',
        path: '/vendor',
        component: VendorListPage,
        meta: {
          auth: true,
          permission: Permissions.values.vendorView,
        },
      },
      {
        name: 'vendorNew',
        path: '/vendor/new',
        component: VendorFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.vendorCreate,
        },
      },
      {
        name: 'vendorImporter',
        path: '/vendor/import',
        component: VendorImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.vendorImport,
        },
      },
      {
        name: 'vendorEdit',
        path: '/vendor/:id/edit',
        component: VendorFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.vendorEdit,
        },
        props: true,
      },
      {
        name: 'vendorView',
        path: '/vendor/:id',
        component: VendorViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.vendorView,
        },
        props: true,
      },
    ],
  },
];
