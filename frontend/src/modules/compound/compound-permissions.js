import Permissions from '@/security/permissions';
import PermissionChecker from '@/modules/iam/permission-checker';

export class CompoundPermissions {
  constructor(currentUser) {
    const permissionChecker = new PermissionChecker(
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.compoundRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.compoundImport,
    );
    this.compoundAutocomplete = permissionChecker.match(
      Permissions.values.compoundAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.compoundCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.compoundEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.compoundDestroy,
    );
  }
}
