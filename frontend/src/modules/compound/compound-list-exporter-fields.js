import { CompoundModel } from '@/modules/compound/compound-model';

const { fields } = CompoundModel;

export default [
  fields.id,
  fields.name,
  fields.unit,
  fields.createdAt
];
