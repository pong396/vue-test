import importerStore from '@/shared/importer/importer-store';
import { CompoundService } from '@/modules/compound/compound-service';
import compoundImporterFields from '@/modules/compound/compound-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  CompoundService.import,
  compoundImporterFields,
  i18n('entities.compound.importer.fileName'),
  i18n('entities.compound.importer.hint'),
);
