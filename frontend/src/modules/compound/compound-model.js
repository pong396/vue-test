import { i18n } from '@/i18n';
import IdField from '@/shared/fields/id-field';
import DateTimeRangeField from '@/shared/fields/date-time-range-field';
import DateTimeField from '@/shared/fields/date-time-field';
import { GenericModel } from '@/shared/model/generic-model';
import IntegerField from '@/shared/fields/integer-field';
import IntegerRangeField from '@/shared/fields/integer-range-field';
import StringField from '@/shared/fields/string-field';
import EnumeratorField from '@/shared/fields/enumerator-field';

function label(name) {
  return i18n(`entities.compound.fields.${name}`);
}

function enumeratorLabel(name, value) {
  return i18n(`entities.compound.enumerators.${name}.${value}`);
}

const fields = {
  id: new IdField('id', label('id')),
  name: new StringField('name', label('name'), {
    "required": true
  }),
  unit: new EnumeratorField('unit', label('unit'), [
    { id: 'IU', label: enumeratorLabel('unit', 'IU') },
    { id: 'mg', label: enumeratorLabel('unit', 'mg') },
    { id: 'mcg', label: enumeratorLabel('unit', 'mcg') },
  ],{
    "required": true
  }),
  standardDose: new IntegerField('standardDose', label('standardDose'), {
    "required": true
  }),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),
  standardDoseRange: new IntegerRangeField(
    'standardDoseRange',
    label('standardDoseRange'),
  ),
};

export class CompoundModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
