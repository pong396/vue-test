import compoundListStore from '@/modules/compound/compound-list-store';
import compoundViewStore from '@/modules/compound/compound-view-store';
import compoundImporterStore from '@/modules/compound/compound-importer-store';
import compoundFormStore from '@/modules/compound/compound-form-store';
import compoundDestroyStore from '@/modules/compound/compound-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: compoundDestroyStore,
    form: compoundFormStore,
    list: compoundListStore,
    view: compoundViewStore,
    importer: compoundImporterStore,
  },
};
