import routes from '@/modules/compound/compound-routes';
import store from '@/modules/compound/compound-store';

export default {
  routes,
  store,
};
