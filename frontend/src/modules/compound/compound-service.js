import gql from 'graphql-tag';
import graphqlClient from '@/shared/graphql/client';

export class CompoundService {
  static async update(id, data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUND_UPDATE(
          $id: String!
          $data: CompoundInput!
        ) {
          compoundUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.compoundUpdate;
  }

  static async destroyAll(ids) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUND_DESTROY($ids: [String!]!) {
          compoundDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.compoundDestroy;
  }

  static async create(data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUND_CREATE($data: CompoundInput!) {
          compoundCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.compoundCreate;
  }

  static async import(values, importHash) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUND_IMPORT(
          $data: CompoundInput!
          $importHash: String!
        ) {
          compoundImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.compoundImport;
  }

  static async find(id) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUND_FIND($id: String!) {
          compoundFind(id: $id) {
            id
            name
            unit
            standardDose
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.compoundFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUND_LIST(
          $filter: CompoundFilterInput
          $orderBy: CompoundOrderByEnum
          $limit: Int
          $offset: Int
        ) {
          compoundList(
            filter: $filter
            orderBy: $orderBy
            limit: $limit
            offset: $offset
          ) {
            count
            rows {
              id
              name
              unit
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.compoundList;
  }

  static async listAutocomplete(query, limit) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUND_AUTOCOMPLETE(
          $query: String
          $limit: Int
        ) {
          compoundAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.compoundAutocomplete;
  }
}
