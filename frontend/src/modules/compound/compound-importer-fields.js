import { CompoundModel } from '@/modules/compound/compound-model';

const { fields } = CompoundModel;

export default [
  fields.name,
  fields.unit,
  fields.standardDose,
];
