import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const CompoundListPage = () =>
  import('@/modules/compound/components/compound-list-page.vue');
const CompoundFormPage = () =>
  import('@/modules/compound/components/compound-form-page.vue');
const CompoundViewPage = () =>
  import('@/modules/compound/components/compound-view-page.vue');
const CompoundImporterPage = () =>
  import('@/modules/compound/components/compound-importer-page.vue');

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'compound',
        path: '/compound',
        component: CompoundListPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundView,
        },
      },
      {
        name: 'compoundNew',
        path: '/compound/new',
        component: CompoundFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundCreate,
        },
      },
      {
        name: 'compoundImporter',
        path: '/compound/import',
        component: CompoundImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundImport,
        },
      },
      {
        name: 'compoundEdit',
        path: '/compound/:id/edit',
        component: CompoundFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundEdit,
        },
        props: true,
      },
      {
        name: 'compoundView',
        path: '/compound/:id',
        component: CompoundViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundView,
        },
        props: true,
      },
    ],
  },
];
