import Permissions from '@/security/permissions';
import PermissionChecker from '@/modules/iam/permission-checker';

export class CompoundLotPermissions {
  constructor(currentUser) {
    const permissionChecker = new PermissionChecker(
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.compoundLotRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.compoundLotImport,
    );
    this.compoundLotAutocomplete = permissionChecker.match(
      Permissions.values.compoundLotAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.compoundLotCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.compoundLotEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.compoundLotDestroy,
    );
  }
}
