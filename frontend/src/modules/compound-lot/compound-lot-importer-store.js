import importerStore from '@/shared/importer/importer-store';
import { CompoundLotService } from '@/modules/compound-lot/compound-lot-service';
import compoundLotImporterFields from '@/modules/compound-lot/compound-lot-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  CompoundLotService.import,
  compoundLotImporterFields,
  i18n('entities.compoundLot.importer.fileName'),
  i18n('entities.compoundLot.importer.hint'),
);
