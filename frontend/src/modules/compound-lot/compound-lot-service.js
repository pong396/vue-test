import gql from 'graphql-tag';
import graphqlClient from '@/shared/graphql/client';

export class CompoundLotService {
  static async update(id, data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUNDLOT_UPDATE(
          $id: String!
          $data: CompoundLotInput!
        ) {
          compoundLotUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.compoundLotUpdate;
  }

  static async destroyAll(ids) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUNDLOT_DESTROY($ids: [String!]!) {
          compoundLotDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.compoundLotDestroy;
  }

  static async create(data) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUNDLOT_CREATE($data: CompoundLotInput!) {
          compoundLotCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.compoundLotCreate;
  }

  static async import(values, importHash) {
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation COMPOUNDLOT_IMPORT(
          $data: CompoundLotInput!
          $importHash: String!
        ) {
          compoundLotImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.compoundLotImport;
  }

  static async find(id) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUNDLOT_FIND($id: String!) {
          compoundLotFind(id: $id) {
            id
            vendorId {
              id
            }
            compoundId {
              id
            }
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.compoundLotFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUNDLOT_LIST(
          $filter: CompoundLotFilterInput
          $orderBy: CompoundLotOrderByEnum
          $limit: Int
          $offset: Int
        ) {
          compoundLotList(
            filter: $filter
            orderBy: $orderBy
            limit: $limit
            offset: $offset
          ) {
            count
            rows {
              id
              vendorId {
                id
              }
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.compoundLotList;
  }

  static async listAutocomplete(query, limit) {
    const response = await graphqlClient.query({
      query: gql`
        query COMPOUNDLOT_AUTOCOMPLETE(
          $query: String
          $limit: Int
        ) {
          compoundLotAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.compoundLotAutocomplete;
  }
}
