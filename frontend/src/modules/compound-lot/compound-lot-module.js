import routes from '@/modules/compound-lot/compound-lot-routes';
import store from '@/modules/compound-lot/compound-lot-store';

export default {
  routes,
  store,
};
