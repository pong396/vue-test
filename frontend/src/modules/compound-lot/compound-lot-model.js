import { i18n } from '@/i18n';
import IdField from '@/shared/fields/id-field';
import DateTimeRangeField from '@/shared/fields/date-time-range-field';
import DateTimeField from '@/shared/fields/date-time-field';
import { GenericModel } from '@/shared/model/generic-model';
import { VendorField } from '@/modules/vendor/vendor-field';
import { CompoundField } from '@/modules/compound/compound-field';

function label(name) {
  return i18n(`entities.compoundLot.fields.${name}`);
}

const fields = {
  id: new IdField('id', label('id')),
  vendorId: VendorField.relationToOne('vendorId', label('vendorId'), {
    "required": true
  }),
  compoundId: CompoundField.relationToOne('compoundId', label('compoundId'), {}),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),

};

export class CompoundLotModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
