import compoundLotListStore from '@/modules/compound-lot/compound-lot-list-store';
import compoundLotViewStore from '@/modules/compound-lot/compound-lot-view-store';
import compoundLotImporterStore from '@/modules/compound-lot/compound-lot-importer-store';
import compoundLotFormStore from '@/modules/compound-lot/compound-lot-form-store';
import compoundLotDestroyStore from '@/modules/compound-lot/compound-lot-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: compoundLotDestroyStore,
    form: compoundLotFormStore,
    list: compoundLotListStore,
    view: compoundLotViewStore,
    importer: compoundLotImporterStore,
  },
};
