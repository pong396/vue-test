import { CompoundLotModel } from '@/modules/compound-lot/compound-lot-model';

const { fields } = CompoundLotModel;

export default [
  fields.id,

  fields.createdAt
];
