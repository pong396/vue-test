import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const CompoundLotListPage = () =>
  import('@/modules/compound-lot/components/compound-lot-list-page.vue');
const CompoundLotFormPage = () =>
  import('@/modules/compound-lot/components/compound-lot-form-page.vue');
const CompoundLotViewPage = () =>
  import('@/modules/compound-lot/components/compound-lot-view-page.vue');
const CompoundLotImporterPage = () =>
  import('@/modules/compound-lot/components/compound-lot-importer-page.vue');

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'compoundLot',
        path: '/compound-lot',
        component: CompoundLotListPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundLotView,
        },
      },
      {
        name: 'compoundLotNew',
        path: '/compound-lot/new',
        component: CompoundLotFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundLotCreate,
        },
      },
      {
        name: 'compoundLotImporter',
        path: '/compound-lot/import',
        component: CompoundLotImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundLotImport,
        },
      },
      {
        name: 'compoundLotEdit',
        path: '/compound-lot/:id/edit',
        component: CompoundLotFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundLotEdit,
        },
        props: true,
      },
      {
        name: 'compoundLotView',
        path: '/compound-lot/:id',
        component: CompoundLotViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.compoundLotView,
        },
        props: true,
      },
    ],
  },
];
