import routes from '@/modules/order/order-routes';
import store from '@/modules/order/order-store';

export default {
  routes,
  store,
};
