import routes from '@/modules/customer/customer-routes';
import store from '@/modules/customer/customer-store';

export default {
  routes,
  store,
};
